#/usr/bin/python
# coding=utf-8

'''
Convert Mathews font data from punched cards to SVG outlines

    Unless otherwise specified, card data is expected in './cards.txt'
    Default behavior outputs a sample sentence for all 3 fonts as .SVG

@author: Jon Bailey
@copyright: Copyright 2017, Jonathan Bailey
@license: Affero GNU General Public License, version 3 or any later version
'''

from __future__ import print_function
import code


class CardParser:
    '''
    The structure of the font data is detailed in [1].
    Thanks to Massimo Petrozzi and Al Kossow of the Computer History Museum for
    digitizing the card deck, found in [2].

    1. Mathews, Max V., Carol Lochbaum, and Judith A. Moss. "Three fonts of
       computer-drawn letters." Visible Language 1.4 (1967): 345-356.
       also published in The Journal of Typographic Research, Volume I, Number
       4, October 1967, pp345-356
       https://s3-us-west-2.amazonaws.com/visiblelanguage/pdf/V1N4_1967_E.pdf
    2. "Vector generated stylized alphabetic font card set", Catalog Number
       102675897, Computer History Museum
       http://www.computerhistory.org/collections/catalog/102675897
    '''

    deck = []  # List of punched cards in order
    # Three font dicts, indexed by character
    fonts = [{'height': 18}, {'height': 27}, {'height': 36}]
    # code. TODO: Store in more descriptive class.
    warnings = {}  # Dict of unique keys to prevent duplicate warnings
    output_buffer = []  # List of lines to print to outfile
    header = footer = False  # Flags for output_buffer
    x_offset = y_offset = 0  # Offsets for adding characters to buffer
    x_max = y_max = 0  # Dimensions for SVG view box to display output

    code_glyphs = {  # See Table II from [1]
        25: 'a', 32: 'b', 41: 'c', 43: 'd', 42: 'e', 13: 'f', 15: 'g',
        34: 'h', 24: 'i', 14: 'j', 40: 'k', 35: 'l', 31: 'm', 44: 'n',
        19: 'o', 10: 'p', 8: 'q', 27: 'r', 18: 's', 46: 't', 45: 'u',
        29: 'v', 16: 'w', 47: 'x', 2: 'y', 62: 'z',
        89: 'A', 96: 'B', 105: 'C', 107: 'D', 106: 'E', 77: 'F', 79: 'G',
        98: 'H', 88: 'I', 78: 'J', 104: 'K', 99: 'L', 95: 'M', 108: 'N',
        83: 'O', 74: 'P', 72: 'Q', 91: 'R', 82: 'S', 110: 'T', 109: 'U',
        93: 'V', 80: 'W', 111: 'X', 66: 'Y', 126: 'Z',
        53: 'ﬀ', 54: 'ﬁ', 55: 'ﬂ', 118: 'ﬃ', 119: 'ﬄ',
        63: '1', 60: '2', 61: '3', 51: '4', 58: '5', 56: '6', 59: '7',
        57: '8', 48: '9', 50: '0',
        123: '&', 26: '\'', 121: '*', 124: '@', 30: '[', 120: '¢', 75: ':',
        9: ',', 0: '-', 115: '$', 12: '=', 125: '#', 112: '(', 114: ')',
        122: '%', 28: '.', 76: '+', 127: '±', 67: '?', 90: '"', 11: ';',
        3: '/', 4: ' ', 64: '_'
    }

    code_widths = {
        25: (9, 12, 16), 32: (9, 16, 18), 41: (8, 13, 16), 43: (9, 16, 19),
        42: (9, 13, 16), 13: (8, 11, 14), 15: (10, 13, 19), 34: (10, 17, 20),
        24: (5, 9, 11), 14: (6, 9, 13), 40: (9, 15, 20), 35: (6, 10, 12),
        31: (15, 25, 30), 44: (10, 17, 21), 19: (9, 14, 18), 10: (9, 16, 21),
        8: (9, 14, 22), 27: (8, 12, 16), 18: (7, 9, 12), 46: (7, 10, 13),
        45: (10, 17, 22), 29: (10, 14, 20), 16: (13, 19, 27), 47: (9, 15, 21),
        2: (9, 15, 19), 62: (7, 12, 15),
        89: (14, 22, 27), 96: (12, 18, 23), 105: (11, 19, 25), 107: (13, 22, 27),
        106: (11, 17, 22), 77: (10, 17, 20), 79: (13, 20, 27), 98: (12, 21, 29),
        88: (7, 11, 14), 78: (9, 15, 18), 104: (12, 19, 25), 99: (11, 18, 24),
        95: (15, 26, 32), 108: (14, 22, 28), 83: (12, 19, 27), 74: (11, 20, 22),
        72: (13, 20, 29), 91: (13, 20, 26), 82: (8, 13, 17), 110: (13, 20, 28),
        109: (13, 22, 28), 93: (13, 20, 26), 80: (18, 27, 36), 111: (13, 21, 28),
        66: (11, 20, 25), 126: (10, 16, 21),
        53: (13, 17, 21), 54: (9, 16, 18), 55: (10, 16, 19), 118: (15, 22, 28),
        119: (15, 23, 28),
        63: (8, 13, 17), 60: (8, 13, 17), 61: (8, 13, 17), 51: (8, 13, 17),
        58: (8, 13, 17), 56: (8, 13, 17), 59: (8, 13, 17), 57: (8, 13, 17),
        48: (8, 13, 17), 50: (8, 13, 17),
        123: (11, 17, None), 26: (4, 6, 7), 121: (8, 10, None), 124: (16, 25, None),
        30: (5, 8, 10), 120: (8, 13, None), 75: (5, 6, 8), 9: (4, 7, 7),
        0: (5, 8, 10), 115: (8, 15, None), 12: (12, 15, None), 125: (12, 21, None),
        112: (5, 7, 8), 114: (5, 8, 9), 122: (13, 21, None), 28: (5, 8, 8),
        76: (12, 16, None), 127: (12, 16, None), 67: (7, 12, 15), 90: (5, 8, 8),
        11: (4, 6, 7), 3: (9, 14, 17), 4: (7, 11, 15), 64: (10, 15, None)
    }

    code_glyphs_e = {  # Extended 'E' character codes
        84: '|', 74: '∠', 54: '≐', 58: ']', 85: '℅', 80: '†', 56: '°',
        75: '÷', 57: '!', 65: '≡', 66: '↔', 67: '→', 73: '∆', 59: '∞',
        83: '∫', 52: '<', 51: '≤', 60: '−', 55: '>', 53: '≥', 82: '×',
        50: '≠', 81: '∥', 77: '(', 76: ')', 79: '∂', 78: '⊥', 63: '“',
        64: '”', 61: '‘', 62: '’', 72: '√', 49: '∅', 69: '⊆', 71: '⊇',
        68: '⊂', 70: '⊃',
        1: 'α', 2: 'β', 3: 'γ', 4: 'δ', 5: 'ε', 6: 'ζ', 7: 'η',
        8: 'θ', 9: 'ι', 10: 'κ', 11: 'λ', 12: 'μ', 13: 'ν', 14: 'ξ',
        15: 'ο', 16: 'π', 17: 'ρ', 18: 'σ', 19: 'τ', 20: 'υ', 21: 'φ',
        22: 'χ', 23: 'ψ', 24: 'ω',
        25: 'Α', 26: 'Β', 27: 'Γ', 28: 'Δ', 29: 'Ε', 30: 'Ζ', 31: 'Η',
        32: 'Θ', 33: 'Ι', 34: 'Κ', 35: 'Λ', 36: 'Μ', 37: 'Ν', 38: 'Ξ',
        39: 'Ο', 40: 'Π', 41: 'Ρ', 42: 'Σ', 43: 'Τ', 44: 'Υ', 45: 'Φ',
        46: 'Χ', 47: 'Ψ', 48: 'Ω'
    }

    code_widths_e = {
        84: (None, 4, None), 74: (None, 17, None), 54: (12, 15, None), 58: (5, 8, 10),
        85: (None, 20, None), 80: (None, 14, None), 56: (7, 8, None), 75: (None, 16, None),
        57: (6, 8, 7), 65: (None, 15, None), 66: (None, 15, None), 67: (None, 15, None),
        73: (None, 16, None), 59: (24, 32, None), 83: (None, 13, None), 52: (13, 15, None),
        51: (13, 15, None), 60: (9, 14, None), 55: (13, 15, None), 53: (13, 15, None),
        82: (None, 13, None), 50: (12, 15, None), 81: (None, 8, None), 77: (None, 5, None),
        76: (None, 5, None), 79: (None, 15, None), 78: (None, 16, None), 63: (None, 12, 14),
        64: (None, 12, 14), 61: (None, 6, 7), 62: (None, 6, 7), 72: (None, 11, None),
        49: (13, 21, None), 69: (None, 15, None), 71: (None, 16, None), 68: (None, 16, None),
        70: (None, 17, None),
        1: (11, 15, None), 2: (13, 19, None), 3: (11, 17, None), 4: (9, 15, None),
        5: (7, 10, None), 6: (8, 11, None), 7: (10, 16, None), 8: (10, 14, None),
        9: (6, 8, None), 10: (9, 12, None), 11: (10, 14, None), 12: (12, 17, None),
        13: (9, 13, None), 14: (10, 14, None), 15: (9, 13, None), 16: (12, 17, None),
        17: (10, 16, None), 18: (12, 18, None), 19: (9, 13, None), 20: (9, 14, None),
        21: (12, 18, None), 22: (12, 17, None), 23: (13, 18, None), 24: (12, 18, None),
        25: (14, 22, None), 26: (12, 18, None), 27: (11, 18, None), 28: (12, 19, None),
        29: (11, 17, None), 30: (10, 16, None), 31: (12, 21, None), 32: (12, 19, None),
        33: (7, 11, None), 34: (12, 19, None), 35: (14, 19, None), 36: (15, 26, None),
        37: (14, 22, None), 38: (13, 18, None), 39: (12, 19, None), 40: (14, 21, None),
        41: (11, 20, None), 42: (12, 19, None), 43: (13, 20, None), 44: (13, 20, None),
        45: (11, 20, None), 46: (13, 21, None), 47: (15, 22, None), 48: (15, 19, None)
    }

    def __init__(self, args=None):
        self.args = args

    def err(self, msg):
        import sys

        print(msg, file=sys.stderr)

    def read_and_count(self):
        """ Read a file of punch-card data, 1/line, skip comments, verify ordering """
        last_card = None
        deck_id = None

        with open(self.args.infile, 'r') as fd:
            for line in fd:
                # Data cards are 80 data columns + EOL, others are comment
                # cards
                if len(line) != 81:
                    if self.args.verbose:
                        self.err(line.rstrip())
                    continue

                card_deck_id = line[72:76]
                card_number = int(line[76:80])

                if deck_id is None:  # First non-comment card - initialize
                    deck_id = card_deck_id
                    last_card = card_number
                else:
                    if card_deck_id != deck_id:
                        raise ValueError('Got card deck ID {}, expected {}'.
                                         format(card_deck_id, deck_id))

                    if card_number != (last_card + 1):
                        raise ValueError('Card sequence error, expected {} got {}'.
                                         format(last_card + 1, card_number))
                    else:
                        last_card = card_number

                self.deck.append(line[0:76])

        # Add cards for space glyphs
        self.deck.append(
            ' 1   4 7                                                                ')
        self.deck.append(
            ' 2   411                                                                ')
        self.deck.append(
            ' 3   415                                                                ')

    def parse(self, card):
        """ Read fields from card per column descriptions in [1] """
        font_num = int(card[0:2]) - 1
        e = True if card[2:3] == 'E' else False
        glyph_code = int(card[3:6])
        glyph_width = int(card[6:8])

        font = self.fonts[font_num]
        (glyphs, widths) = (self.code_glyphs_e, self.code_widths_e) if e else \
            (self.code_glyphs, self.code_widths)

        if (glyph_code not in glyphs):
            raise ValueError('Unexpected glyph_code {} {}'.format('E' if e else ' ',
                                                                  glyph_code))

        if widths[glyph_code][font_num] != glyph_width and self.args.verbose:
            warning = 'Unexpected width for font {} glyph {}{}, expected {} got {}'.format(
                font_num + 1, 'E' if e else ' ', glyph_code, widths[glyph_code][font_num], glyph_width)
            if warning not in self.warnings:
                self.err(warning)
            self.warnings[warning] = True

        ext_glyph_code = '{}{}'.format(glyph_code, 'E' if e else '')

        if ext_glyph_code not in font:
            font[ext_glyph_code] = {
                'strokes': [],
                'glyph': glyphs[glyph_code],
                'width': glyph_width}

        #
        # Extract up to 8 line strokes per card
        # Each stroke is stored as 'AABBCCDD', indicating a line from AA,BB to
        # CC,DD with an origin of 0,0 in the top left.
        #
        for offset in [8, 16, 24, 32, 40, 48, 56, 64]:
            stroke = card[offset:offset + 8]
            if stroke == '        ':  # Padding
                continue
            (x1, y1) = (int(stroke[0:2]), int(stroke[2:4]))
            (x2, y2) = (int(stroke[4:6]), int(stroke[6:8]))
            font[ext_glyph_code]['strokes'].append(((x1, y1), (x2, y2)))

    def inventory(self):
        """ Check totals for glyph data, character data, and parsed font data """
        glyph_total = width_total = font_total = e = 0
        missing = []
        missings = ''

        for glyphset in (self.code_glyphs, self.code_glyphs_e):
            glyph_total += len(glyphset)
        for widthset in (self.code_widths, self.code_widths_e):
            for triple in widthset:
                i = 0  # Count which font we're in
                for entry in widthset[triple]:
                    glyphstr = str(triple) + ('E' if e else '')
                    if entry is not None:
                        width_total += 1
                    i += 1
            e += 1

        for font in self.fonts:
            font_total += len(font)

        if self.args.verbose:
            missing_count = 0
            for item in missing:
                missing_count += 1
                print('Missing character #{} {}'.format(missing_count, item))
            print('{} characters as {} glyphs expected, {} (+3 SPACEs) parsed.'.format(
                glyph_total, width_total, font_total))

    def prettyprint(self, arg):
        import pprint
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(arg)

    def find_glyph(self, in_glyph):
        """ Return a glyph's code - https://stackoverflow.com/a/14624923 """

        for code in self.code_glyphs:
            if self.code_glyphs[code] == in_glyph:
                return code
        for code in self.code_glyphs_e:
            if self.code_glyphs_e[code] == in_glyph:
                return '{}E'.format(code)
        raise ValueError('Glyph {} not found'.format(in_glyph))

    def add_text(self, font_num=1, text=None):
        """ Add a line of text to output_buffer
            arguments: font_num - 1, 2, or 3 for the small / med / large font
                       text - A Unicode block of text to output
        """
        if self.header == False:
            self.output_buffer.append(
                '<svg version="1.1" xmlns="http://www.w3.org/2000/svg">')
            # TODO: add viewBox="0 0 xx yy"
            self.header = True
        font = self.fonts[font_num - 1]
        for letter in text:
            code = str(self.find_glyph(letter))
            strokes = font[code]['strokes']

            output_line = '<path fill="none" stroke="#000" stroke-linecap="round" stroke-width="2.6" d="'

            for stroke in strokes:
                ((x1, y1), (x2, y2)) = stroke
                x1 += self.x_offset
                y1 += self.y_offset
                x2 += self.x_offset
                y2 += self.y_offset
                output_line += 'M{} {} {} {}'.format(x1, y1, x2, y2)

            if len(strokes) > 0:  # That is, not a space
                output_line += '" />'
                self.output_buffer.append(output_line)

            width = font[code]['width']
            self.x_offset += width
            if self.x_offset > self.x_max:
                self.x_max = self.x_offset

        self.y_offset += font['height']
        self.x_offset = 0
        self.y_max += self.y_offset

    def output(self, output_format='svg'):
        """ Send the output buffer to a file, appending a footer if necessary """
        if self.footer == False:
            self.output_buffer.append('</svg>')

        with open(self.args.outfile, 'x') as fd:
            for line in self.output_buffer:
                print(line, file=fd)

    def print_sampler(self):
        self.add_text(
            font_num=3,
            text=' PACK MY BOX WITH FIVE DOZEN LIQUOR JUGS')
        self.add_text(
            font_num=3,
            text='   how quickly daft jumping zebras vex')
        self.add_text(
            font_num=2,
            text=' JACKDAWS LOVE MY BIG SPHINX OF QUARTZ')
        self.add_text(
            font_num=2,
            text='   sphinx of black quartz: judge my vow')
        self.add_text(
            font_num=1,
            text=' QUICK ZEPHYRS BLOW, VEXING DAFT JIM')
        self.add_text(
            font_num=1,
            text='   waltz, nymph, for quick jigs vex bud')

        # Output every character in every font.
        def sorter(arg):
            if arg.endswith('E'):
                return 1000 + int(arg.rstrip('E'))
            return int(arg)
        line = '    '
        for fontid in (0, 1, 2):
            keylist = list(self.fonts[fontid].keys())
            keylist.remove('height')
            keylist.sort(key=sorter)
            for key in keylist:
                line += ' {}'.format(self.fonts[fontid][key]['glyph'])
                if len(line) == 44:
                    self.add_text(font_num=(fontid + 1), text=line)
                    line = '    '
            self.add_text(font_num=(fontid + 1), text=line)
            line = '    '

        self.output(output_format='svg')

    def parse_cards(self):
        self.read_and_count()

        for card in self.deck:
            self.parse(card)

        self.inventory()

        # TODO: Check for args for different outputs.

        self.print_sampler()


def main():

    import argparse

    p = argparse.ArgumentParser()

    p.add_argument('-i', '--infile', default='cards.txt')
    p.add_argument('-o', '--outfile', default='sample.svg')
    p.add_argument('-v', '--verbose', action='store_true')

    args = p.parse_args()

    cp = CardParser(args)
    cp.parse_cards()


if (__name__ == '__main__'):
    main()
